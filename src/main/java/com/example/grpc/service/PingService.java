package com.example.grpc.service;

import com.example.grpc.model.PingRequest;
import com.example.grpc.model.PingResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by thinhda.
 * Date: 2019-08-18
 */

@Service
@Slf4j
public class PingService {
    public PingResponse ping(PingRequest request) {
        return PingResponse.newBuilder()
            .setTimestamp(request.getTimestamp())
            .setMessage("Pong")
            .build();
    }
}
